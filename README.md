# Keil MDK5 STM32L4xx 官方固件库驱动库板级支持包

## 资源文件介绍

### 文件名
Keil.STM32L4xx_DFP.2.6.0.pack

### 版本信息
- **版本**: 2.6.0
- **发布日期**: 2021-07-30

### 文件描述
该资源文件是针对Keil MDK5的STM32L4xx系列微控制器的官方固件库驱动库板级支持包（DFP）。该包基于STM32Cube_FW_L4固件包的V1.17.0版本进行了更新。

### 主要更新内容
- **全局定义**: 在组件 `::Device:STM32Cube LL:Common` 中添加了全局定义 `USE_FULL_LL_DRIVER`。
- **调试探针板描述**: 添加了调试探针板的描述。
- **设备支持**:
  - 更新了SVD文件和文档。
  - 重新调整了设备RAM大小。
  - 新增支持的设备：
    - STM32L451CETx
    - STM32L452CETx
    - STM32L462CETx
    - STM32L4Q5CGTxP
    - STM32L4Q5CGUxP
    - STM32L4Q5VGTxP
    - STM32L4Q5QGIxP
    - STM32L4Q5ZGTxP
    - STM32L4Q5AGIxP
    - STM32L4Q5RGTxP
  - 移除的设备：
    - STM32L451RCYx
    - STM32L452RCYx
    - STM32L485xx
- **dbgconf文件**: 更新了dbgconf文件，添加了缺失的变量 `DoOptionByteLoading`。
- **CMSIS Flash算法**:
  - 为STM32L4RxxG设备添加了新的算法。
  - 更新了以下设备的Flash算法：
    - STM32L4Rx_2048_Dual
    - STM32L4P5xx_1M
    - STM32L4P5xx_512
  - 修复了在编程时禁用中断的问题，并修复了最后一个字的编程问题。
- **CMSIS-Driver**:
  - **USBH**: 修复了端口恢复偶尔卡在恢复信号的问题，并添加了编译时配置以减少Bulk IN NAK率。
  - **SPI**: 更新了控制函数，仅在主模式下设置SPI总线速度，并在重新配置DMA之前禁用DMA。更新了 `SPI_GetStatus` 函数以返回正确的错误代码，并更新了 `SPI_GetDataCount` 函数以在SPI未初始化时返回正确的值。
- **板级示例**: 更新了汇编选项以支持armclang（自动选择），并更新了配置文件以支持CMSIS 5.8.0和MDK-Middleware 7.13.0。

### 使用说明
直接运行该资源文件即可安装或更新Keil MDK5的STM32L4xx系列固件库驱动库板级支持包。

### 注意事项
- 请确保您的Keil MDK5版本与该支持包兼容。
- 在安装或更新前，建议备份现有项目和配置文件。

### 相关链接
- [STM32L4xx系列官方网站](https://www.st.com/en/microcontrollers-microprocessors/stm32l4-series.html)
- [Keil MDK5官方网站](https://www.keil.com/mdk5/)

### 许可证
该资源文件遵循STMicroelectronics的许可证协议。请在使用前仔细阅读相关许可证条款。

### 贡献与反馈
如果您在使用过程中遇到任何问题或有改进建议，欢迎通过GitHub提交Issue或Pull Request。

---

**感谢您的使用与支持！**